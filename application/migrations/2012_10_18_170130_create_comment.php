<?php

class Create_Comment {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('comments', function($table) {
			$table->increments('id');
			$table->integer('post_id');
			$table->text('body');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('comments');
	}

}
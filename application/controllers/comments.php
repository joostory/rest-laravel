<?php
class Comments_Controller extends Base_Controller {
	public $restful = true;

	public function get_index($post_id, $id = null) {
		if(empty($id)) {
			$comments = Comment::all($post_id);
			return json_encode($comments);
		} else {
			$comment = Comment::find($post_id, $id);
			return json_encode($comment);
		}
	}

	public function post_index($post_id) {
		$param = Input::json();
		Comment::insert((array)$param);
		return json_encode($param);
	}

	public function put_index($post_id, $id) {
		$param = Input::json();
		Comment::update($post_id, $id, (array)$param);
		return json_encode($param);
	}

	public function delete_index($post_id, $id) {
		Comment::delete($post_id, $id);
		return;
	}
}
?>
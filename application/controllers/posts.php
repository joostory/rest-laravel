<?php
class Posts_Controller extends Base_Controller {
	public $restful = true;

	public function get_index($id = null) {
		if(empty($id)) {
			$posts = Post::all();
			return json_encode($posts);
		} else {
			$post = Post::find($id);
			return json_encode($post);
		}
	}

	public function post_index() {
		$param = Input::json();
		Post::insert((array)$param);
		return json_encode($param);
	}

	public function put_index($id) {
		$param = Input::json();
		Post::update($id, (array)$param);
		return json_encode($param);
	}

	public function delete_index($id = null) {
		Post::delete($id);
		return;
	}
}
?>
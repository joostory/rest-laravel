<?php
class Comment {
	public function post() {
		return $this->belongs_to('Post');
	}

	public static function find($post_id, $id) {
		return DB::table("comments")->where('id', '=', $id)->first();
	}

	public static function all($post_id) {
		return DB::table("comments")->where('post_id', '=', $post_id)->get();
	}

	public static function delete($post_id, $id) {
		return DB::table("comments")->delete($id);
	}

	public static function insert($data) {
		return DB::table("comments")->insert($data);
	}

	public static function update($post_id, $id, $data) {
		return DB::table("comments")->where('id', '=', $id)->update($data);
	}
}
?>
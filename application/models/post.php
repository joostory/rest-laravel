<?php
class Post {

	public static $name = "posts";

	public function comments($id) {
		return DB::table("comments")->where('post_id', '=', $id);
	}

	public static function find($id) {
		return DB::table(self::$name)->find($id);
	}

	public static function all() {
		return DB::table(self::$name)->get();
	}

	public static function delete($id) {
		return DB::table(self::$name)->delete($id);
	}

	public static function insert($data) {
		return DB::table(self::$name)->insert($data);
	}

	public static function update($id, $data) {
		return DB::table(self::$name)->where('id', '=', $id)->update($data);
	}
}
?>